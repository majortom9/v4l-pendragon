/*
 * Driver for the ST STV0910 DVB-S/S2 demodulator.
 *
 * Copyright (C) 2014-2015 Ralph Metzler <rjkm@metzlerbros.de>
 *                         Marcus Metzler <mocm@metzlerbros.de>
 *                         developed for Digital Devices GmbH
 * Copyright (C) 2015-2016 Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016      Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 only, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA
 * Or, point your browser to http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/i2c.h>

#include "stv0910.h"
#include "stv0910_regs.h"
#include "stv0910_priv.h"
#include "stv0910_init.h"

//	!!!WARNING!!! I2C locking required in i2c_transfer() for any demod run in dual mode

#define MAX_XFER_SIZE 64

int stv0910debug = 1;
module_param_named(debug, stv0910debug, int, 0644);

LIST_HEAD(stv0910list);

static int readRegs(struct stv0910_state *state, u16 reg, u8 *data, u8 len) {
	u8 msg[2] = { reg >> 8, reg & 0xff };
	
	struct i2c_msg msgs[] = {
		{ .addr = state->config->adr, .flags = 0,        .buf = msg , .len = 2   },
		{ .addr = state->config->adr, .flags = I2C_M_RD, .buf = data, .len = len }
	};
	
	return i2c_transfer(state->base->i2c, msgs, 2) == 2 ? 0 : -EREMOTEIO;
}

static int readReg(struct stv0910_state *state, u16 reg) {
	u8 data;
	
	return readRegs(state, reg, &data, 1) == 0 ? data : -EREMOTEIO;
}

static u32 readBE(struct stv0910_state *state, u16 reg, u8 len) {
	int i;
	u32 result = 0;
	u8 buf[4];
	
	readRegs(state, reg, buf, len);
	
	for (i = 0; i < len; i++)
		result = (result << 8) | buf[i];
	
	return result;
}

static u64 readBE64(struct stv0910_state *state, u16 reg, u8 len) {
	int i;
	u64 result = 0;
	u8 buf[8];
	
	readRegs(state, reg, buf, len);
	
	for (i = 0; i < len; i++)
		result = (result << 8) | buf[i];
	
	return result;
}

static int writeRegs(struct stv0910_state *state, u16 reg, u8 *data, u8 len) {
	u8 buf[MAX_XFER_SIZE];
	
	struct i2c_msg msg = {
		.addr  = state->config->adr, .flags = 0, .buf = buf, .len = len + 2
	};
	
	buf[0] = reg >> 8;
	buf[1] = reg & 0xff;
	memcpy(&buf[2], data, len);
	
	return i2c_transfer(state->base->i2c, &msg, 1) == 1 ? 0 : -EREMOTEIO;
}

static int writeReg(struct stv0910_state *state, u16 reg, u8 data) {
	return writeRegs(state, reg, &data, 1);
}

#if 0
static int writeBE(struct stv0910_state *state, u16 reg, u32 data, u8 len) {
	int i;
	u8 buf[4];
	
	for (i = len - 1; i >= 0; i--) {
		buf[i] = data & 0xff;
		data >>= 8;
	}
	
	return writeRegs(state, reg, buf, len);
}

static int writeBE64(struct stv0910_state *state, u16 reg, u64 data, u8 len) {
	int i;
	u8 buf[8];
	
	for (i = len - 1; i >= 0; i--) {
		buf[i] = data & 0xff;
		data >>= 8;
	}
	
	return writeRegs(state, reg, buf, len);
}
#endif

//	"Using de Bruijn Sequences to Index a 1 in a Computer Word", Leiserson, Prokop & Randall
static u8 deBruijnHash[] = { 0, 1, 6, 2, 7, 5, 4, 3 };

static inline u8 getOnePos(u8 mask) {
	return deBruijnHash[(u8) ((-mask & mask) * 0x1d) >> 5];
}

static inline u8 extractField(u32 field, u8 val) {
	u8 mask = field & 0xff;
	
	return (val & mask) >> getOnePos(mask);
}

static inline u8 insertField(u32 field, u8 old, u8 new) {
	u8 mask = field & 0xff;
	
	return (old & ~mask) | ((new << getOnePos(mask)) & mask);
}

static u8 readField(struct stv0910_state *state, u32 field) {
	u8 mask = field & 0xff;
	
	return (readReg(state, field >> 16) & mask) >> getOnePos(mask);
}

static int writeField(struct stv0910_state *state, u32 field, u8 val) {
	u16 reg = field >> 16;
	u8 mask = field & 0xff;
	
	return writeReg(state, reg, (readReg(state, reg) & ~mask) | ((val << getOnePos(mask)) & mask));
}

static int demodIsDead(struct stv0910_state *state) {
	return readBE(state, RSTV0910_MID, 2) != 0x5120;
}

static int getAlgo(struct dvb_frontend *fe) {
	return DVBFE_ALGO_CUSTOM;
}

static void updateStandard(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	enum fe_delivery_system delsys;
	
	switch (READfield(HEADER_MODE)) {
		case PLHdetected:
		case DVBS2found:
			delsys = SYS_DVBS2;
			break;
			
		case DVBSfound:
			if (READfield(DSS_DVB))
				delsys = SYS_DSS;
			else
				delsys = SYS_DVBS;
			
			break;
			
		case Searching:
		default:
			delsys = SYS_UNDEFINED;
			break;
	}
	
	p->delivery_system = delsys;
}

static int updateStatus(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u32 status = FE_HAS_SIGNAL;
	u8  reg    = READreg(DSTATUS);;

	if (EXTRACTfield(CAR_LOCK, reg))
		status |= FE_HAS_CARRIER;
	
	if (EXTRACTfield(TMGLOCK_QUALITY, reg) >> 1)
		status |= FE_HAS_TIMING;
	
	if (EXTRACTfield(LOCK_DEFINITIF, reg))
		status |= FE_HAS_LOCK;
	
	updateStandard(state);
	
	switch (p->delivery_system) {
		case SYS_DVBS2:
			if (READfield(PKTDELIN_LOCK))
				status |= FE_HAS_VITERBI | FE_HAS_SYNC;
			break;
			
		case SYS_DVBS:
			reg = READreg(VSTATUSVIT);
			
			if (EXTRACTfield(PRFVIT, reg))
				status |= FE_HAS_VITERBI;
			
			if (EXTRACTfield(LOCKEDVIT, reg))
				status |= FE_HAS_SYNC;
			break;
			
		default:
			break;
	}
	
	if ((status ^ p->status) & FE_HAS_LOCK) {
		if (status & FE_HAS_LOCK)
			dprintk("%s: FULL LOCK %x\n", __func__, status);
		else
			dprintk("%s: UNLOCKED %x\n",  __func__, status);
	}
	
	p->status = status;
	return 0;
}

static u32 getSymbolRate(struct stv0910_state *state) {
	s64 sfr;
	s32 tmg;
	
	sfr = READbe(SFR3, 4) * (s64) state->base->mclk;
	tmg = READbe(TMGREG2, 3);
	
	if (tmg >> 23)
		tmg |= 0xff000000;
	
	return (sfr + ((sfr + (1LL << 28)) >> 29) * tmg + (1LL << 31)) >> 32;
}

static int updateSymbolRate(struct dvb_frontend *fe) {
	fe->dtv_property_cache.symbol_rate = getSymbolRate(fe->demodulator_priv);
	return 0;
}

static s32 getFrequencyOffset(struct stv0910_state *state) {
	return ((u64) state->base->mclk * (s32) (READbe(CFR2, 3) << 8) + (1LL << 31)) / (1LL << 32);
}

static u64 getFrequency(struct stv0910_state *state) {
	return state->fe.tuner.frequency + getFrequencyOffset(state);
}

static int updateFrequency(struct dvb_frontend *fe) {
	fe->dtv_property_cache.frequency = getFrequency(fe->demodulator_priv);
	return 0;
}

static s32 interpolate(const struct LookupTable *table, int size, u16 raw) {
	s32 i, imin, imax, div;
	
	imin = 0;
	imax = size - 1;
	
	for (;;) {
		i = (imax + imin) >> 1;
		
		if (raw > table[i].raw)
			imax = i - 1;
		else if (raw < table[i + 1].raw)
			imin = i + 1;
		else
			break;
	}
	
	//	reversed sign convention to ensure rounding is always positive
	div = table[i].raw - table[i + 1].raw;	
	return ((table[i].real - table[i + 1].real) * (raw - table[i].raw) - (div >> 1)) / div + table[i].real;
}

static s32 getSNR(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	const struct LookupTable *table;
	u16 raw;
	u32 size;

	if (p->delivery_system == SYS_DVBS2) {
		raw   = READbe(NNOSPLHT1, 2);
		table = s2_cnr_lookup;
		size  = sizeof(s2_cnr_lookup) / sizeof(struct LookupTable);
	}
	else {
		raw   = READbe(NNOSDATAT1, 2);
		table = s1_cnr_lookup;
		size  = sizeof(s1_cnr_lookup) / sizeof(struct LookupTable);
	}
	
	return interpolate(table, size, raw);
}

static int updateSNR(struct dvb_frontend *fe) {
	fe->dtv_property_cache.snr = getSNR(fe->demodulator_priv);
	return 0;
}

static s32 getSignalLevel(struct stv0910_state *state) {
	struct tuner_state *tuner = &state->fe.tuner;
	const struct LookupTable *table;
	u32 size, level, power;
	u8 iq[2];
	
	switch (state->config->tunerType) {
		case STV0910_TUNER_STV6111:
			//	no calibration data for stv6111 so use stv6120 values
			table = agc_gain_table_6120;
			size  = sizeof(agc_gain_table_6120) / sizeof(struct LookupTable);
			break;
			
		case STV0910_TUNER_STV6120:
			table = agc_gain_table_6120;
			size  = sizeof(agc_gain_table_6120) / sizeof(struct LookupTable);
			break;
	}

	READregs(POWERI, iq, 2);

	level = -interpolate(table, size, READbe(AGCIQIN1, 2)) - tuner->gain - tuner->tilt;
	power = (u32) iq[0] * iq[0] + (u32) iq[1] * iq[1];
	
	return level + interpolate(adc_power_table, sizeof(adc_power_table) / sizeof(struct LookupTable), power);
}

static int updateSignalStrength(struct dvb_frontend *fe) {
	fe->dtv_property_cache.signal_level = getSignalLevel(fe->demodulator_priv);
	return 0;
}

static u8 getOptimCarLoop(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	int i = p->pilot ? 0 : 1;
	
	if (state->modCode > DVBS2_32APSK_9_10)
		i += (DVBS2_32APSK_9_10 - DVBS2_QPSK_1_4) * 10;
	else if (state->modCode >= DVBS2_QPSK_1_4)
		i += (state->modCode - DVBS2_QPSK_1_4) * 10;
	
	if (p->symbol_rate > 15000000) {
		if (p->symbol_rate > 25000000)
			i += 8;
		else
			i += 6;
	}
	else {
		if (p->symbol_rate > 7000000)
			i += 4;
		else if (p->symbol_rate > 3000000)
			i += 2;
	}
	
	return s2CarLoop[i];
}

static void setPLS(struct stv0910_state *state, u8 pls_mode, u32 pls_code) {
	u8 reg[3];
	
	if (pls_mode == 0 && pls_code == 0)
		pls_code = 1;
	
	reg[0] = ((pls_mode << 2) & 0xc) | ((pls_code >> 16) & 0x3);
	reg[1] = pls_code >> 8;
	reg[2] = pls_code;
	
	WRITEregs(PLROOT2, reg, 3);
}

static void setMIS(struct stv0910_state *state, int mis) {
	u8 reg[2];
	
	if (mis < 0 || mis > 255)
		WRITEfield(FILTER_EN, 0);
	else {
		reg[0] = mis;
		reg[1] = 0xff;
		
		WRITEregs(ISIENTRY, reg, 2);
		WRITEfield(FILTER_EN, 1);
	}
}

static void controlTS(struct stv0910_state *state, int enable) {
	WRITEreg(TSCFGH, state->tscfgh | 0x1);
	
	if (enable)
		WRITEreg(TSCFGH, state->tscfgh);
}

static void setTunerMutex(struct stv0910_state *state, int lock) {
	if (state->config->tunerChips != 2) {
		if (lock)
			mutex_lock(&state->base->tunerLock);
		else
			mutex_unlock(&state->base->tunerLock);
	}
}

static void setDemodMutex(struct stv0910_state *state, int lock) {
	if (!state->config->single) {
		if (lock)
			mutex_lock(&state->base->demodLock);
		else
			mutex_unlock(&state->base->demodLock);
	}
}

static int i2cGateCtrl(struct dvb_frontend *fe, int enable) {
	struct stv0910_state *state = fe->demodulator_priv;
	u16 i2crpt = state->config->tunerI2C ? RSTV0910_P2_I2CRPT : RSTV0910_P1_I2CRPT;
	
	if (enable) {
		setTunerMutex(state, 1);
		writeReg(state, i2crpt, state->i2crpt | 0x80);
	}
	else {
		writeReg(state, i2crpt, state->i2crpt | 0x02);
		setTunerMutex(state, 0);
	}
	
	return 0;
}

static void getLockTimeouts(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u8 level = p->lock_timeout_level;
	
	if (!p->symbol_rate)
		p->symbol_rate = state->fe.ops.info.symbol_rate_min;
	
	state->demodTimeout = 500 + 2500000000LL / p->symbol_rate;
	state->fecTimeout   = state->demodTimeout >> 2;
	
	if (level == 0)
		level  = 5;
	
	if (level > 5) {
		if (level > 7)
			level = 7;
		
		state->demodTimeout <<= level - 5;
	}
	else if (level < 5) {
		if (level < 3)
			level = 3;
		
		state->demodTimeout >>= 5 - level;
	}
	
	p->lock_timeout_level = level;
	p->lock_timeout       = state->demodTimeout + state->fecTimeout;
}

static int setViterbi(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u8 prvit      = 0;
	u8 setThresh  = 0;
	u8 everything = p->delivery_system == SYS_UNDEFINED || p->delivery_system == SYS_S_AUTO;
	
	if (p->delivery_system == SYS_DVBS || everything) {
		setThresh = 1;
		
		switch (p->fec_inner) {
			case FEC_NONE:
			case FEC_AUTO:
				prvit = 0x47;
				break;
				
			case FEC_1_2:
				prvit = 0x01;
				break;
				
			case FEC_2_3:
				prvit = 0x02;
				break;
				
			case FEC_3_4:
				prvit = 0x04;
				break;
				
			case FEC_5_6:
				prvit = 0x08;
				break;
				
			case FEC_7_8:
				prvit = 0x20;
				break;
				
			default:
				dprintk("%s: fec %d not valid for DVB-S\n", __func__, p->fec_inner);
				return -EINVAL;
				break;	   
		}
		
		WRITEreg(SFDLYSET2, 0);
	}
	
	if (p->delivery_system == SYS_DSS || everything) {
		setThresh = 1;
		
		switch (p->fec_inner) {
			case FEC_NONE:
			case FEC_AUTO:
				prvit |= 0x13;
				break;
				
			case FEC_1_2:
				prvit |= 0x01;
				break;
				
			case FEC_2_3:
				prvit |= 0x02;
				break;
				
			case FEC_6_7:
				prvit |= 0x10;
				break;
				
			default:
				dprintk("%s: fec %d not valid for DSS\n", __func__, p->fec_inner);
				return -EINVAL;
				break;	   
		}
	}
	
	WRITEreg(PRVIT, prvit);
	
	if (setThresh) {
		u8 regs[6] = { 0xd7, 0x85, 0x58, 0x3a, 0x34, 0x28 };
		WRITEregs(VTH12, regs, 6);
	}

	return 0;
}

static int setModCodes(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u8 i, modCode[0x10];

	if (p->delivery_system == SYS_DVBS2 || p->delivery_system == SYS_UNDEFINED || p->delivery_system == SYS_S_AUTO) {
		if (p->fec_inner == FEC_AUTO || p->fec_inner == FEC_NONE) {
			for (i = 0; i < 0x10; i++)
				modCode[i] = 0x00;
		}
		else {
			for (i = 0; i < 0x10; i++)
				modCode[i] = 0xff;
			
			if (p->modulation == QPSK || p->modulation == MOD_AUTO) {
				switch (p->fec_inner) {
					case FEC_1_4:
						modCode[0xf] &= 0x0f;
						break;
						
					case FEC_1_3:
						modCode[0xe] &= 0xf0;
						break;
						
					case FEC_2_5:
						modCode[0xe] &= 0x0f;
						break;
						
					case FEC_1_2:
						modCode[0xd] &= 0xf0;
						break;
						
					case FEC_3_5:
						modCode[0xd] &= 0x0f;
						break;
						
					case FEC_2_3:
						modCode[0xc] &= 0xf0;
						break;
						
					case FEC_3_4:
						modCode[0xc] &= 0x0f;
						break;
						
					case FEC_4_5:
						modCode[0xb] &= 0xf0;
						break;
						
					case FEC_5_6:
						modCode[0xb] &= 0x0f;
						break;
						
					case FEC_8_9:
						modCode[0xa] &= 0xf0;
						break;
						
					case FEC_9_10:
						modCode[0xa] &= 0x0f;
						break;
						
					default:
						if (p->modulation != MOD_AUTO) {
							dprintk("%s: fec %d not valid for DVB-S2 QPSK\n", __func__, p->fec_inner);
							return -EINVAL;
						}
						break;
				}
			}
			
			if (p->modulation == PSK_8 || p->modulation == MOD_AUTO) {
				switch (p->fec_inner) {
					case FEC_3_5:
						modCode[0x9] &= 0xf0;
						break;
						
					case FEC_2_3:
						modCode[0x9] &= 0x0f;
						break;
						
					case FEC_3_4:
						modCode[0x8] &= 0xf0;
						break;
						
					case FEC_5_6:
						modCode[0x8] &= 0x0f;
						break;
						
					case FEC_8_9:
						modCode[0x7] &= 0xf0;
						break;
						
					case FEC_9_10:
						modCode[0x7] &= 0x0f;
						break;
						
					default:
						if (p->modulation != MOD_AUTO) {
							dprintk("%s: fec %d not valid for DVB-S2 8PSK\n", __func__, p->fec_inner);
							return -EINVAL;
						}
						break;
				}
			}
			
			if (p->modulation == APSK_16 || p->modulation == MOD_AUTO) {
				switch (p->fec_inner) {
					case FEC_2_3:
						modCode[0x6] &= 0xf0;
						break;
						
					case FEC_3_4:
						modCode[0x6] &= 0x0f;
						break;
						
					case FEC_4_5:
						modCode[0x5] &= 0xf0;
						break;
						
					case FEC_5_6:
						modCode[0x5] &= 0x0f;
						break;
						
					case FEC_8_9:
						modCode[0x4] &= 0xf0;
						break;
						
					case FEC_9_10:
						modCode[0x4] &= 0x0f;
						break;
						
					default:
						if (p->modulation != MOD_AUTO) {
							dprintk("%s: fec %d not valid for DVB-S2 16APSK\n", __func__, p->fec_inner);
							return -EINVAL;
						}
						break;
				}
			}
			
			if (p->modulation == APSK_32 || p->modulation == MOD_AUTO) {
				switch (p->fec_inner) {
					case FEC_3_4:
						modCode[0x3] &= 0xf0;
						break;
						
					case FEC_4_5:
						modCode[0x3] &= 0x0f;
						break;
						
					case FEC_5_6:
						modCode[0x2] &= 0xf0;
						break;
						
					case FEC_8_9:
						modCode[0x2] &= 0x0f;
						break;
						
					case FEC_9_10:
						modCode[0x1] &= 0xf0;
						break;
						
					default:
						if (p->modulation != MOD_AUTO) {
							dprintk("%s: fec %d not valid for DVB-S2 32APSK\n", __func__, p->fec_inner);
							return -EINVAL;
						}
						break;
				}
			}
		}
			
		modCode[0x0] |= 0xff;
		modCode[0x1] |= 0xfc;
		modCode[0x4] |= 0xc0;
		modCode[0x7] |= 0xc0;
		modCode[0xa] |= 0xc0;
		modCode[0xf] |= 0x0f;
	}
	else {
		for (i = 0; i < 0x10; i++)
			modCode[i] = 0xff;
	}
	
	WRITEregs(MODCODLST0, modCode, 0x10);
	
	return 0;
}

static int setSearchStandard(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u8 dmdcfg, rff6d, fecm;
	
	dmdcfg = READreg(DMDCFGMD) & 0x3f;
	fecm   = 0x00;
	
	setDemodMutex(state, 1);
	rff6d = readReg(state, 0xff6d) & ~(1 << state->config->demod);

	switch (p->delivery_system) {
		case SYS_DVBS2:
			dmdcfg |= 0x80;
			rff6d  |= (1 << state->config->demod);
			break;
			
		case SYS_DVBS:
			dmdcfg |= 0x40;
			break;
			
		case SYS_DSS:
			dmdcfg |= 0x40;
			fecm    = 0x80;
			break;
			
		case SYS_S_AUTO:
		case SYS_UNDEFINED:
			dmdcfg |= 0xc0;
			fecm    = 0x10;
			break;
			
		default:
			mutex_unlock(&state->base->demodLock);
			dprintk("%s: delivery system %d not supported\n", __func__, p->delivery_system);
			return -EINVAL;
	}
	
	writeReg(state, 0xff6d, rff6d);
	setDemodMutex(state, 0);
	
	WRITEreg(DMDCFGMD, dmdcfg);
	WRITEreg(FECM, fecm);
	
	return 0;
}

static int tune(struct stv0910_state *state, u32 searchRange) {
	struct dvb_frontend *fe = &state->fe;
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	struct dvb_tuner_ops *tuner_ops = &state->fe.ops.tuner_ops;
	struct tuner_state *tuner = &fe->tuner;
	u32 roll   = 0;
	int result = 0;
	
	switch (p->rolloff) {
		case ROLLOFF_AUTO:
		case ROLLOFF_35:
			roll = 35;
			break;
			
		case ROLLOFF_25:
			roll = 25;
			break;
			
		case ROLLOFF_20:
			roll = 20;
			break;
			
		default:
			dprintk("%s: rolloff %d not supported\n", __func__, p->rolloff);
			return -EINVAL;
			break;
	}
	
	p->bandwidth_hz = (u64) p->symbol_rate * (100 + roll) / 100 + searchRange;
	
	if (p->bandwidth_hz > 72000000)
		p->bandwidth_hz = 72000000;
	
	i2cGateCtrl(fe, 1);
	
	if (tuner_ops->set_params) {
		tuner->frequency_cmd = p->frequency;
		tuner->bandwidth_cmd = p->bandwidth_hz;
		result = tuner_ops->set_params(fe);
	}
	else {
		if (tuner_ops->set_frequency)
			result = tuner_ops->set_frequency(fe, (p->frequency + 500) / 1000);
		if (tuner_ops->set_bandwidth)
			result |= tuner_ops->set_bandwidth(fe, p->bandwidth_hz);
	}
	
	i2cGateCtrl(fe, 0);
	
	return result;
}

static int waitTunerLock(struct stv0910_state *state) {
	struct dvb_frontend *fe = &state->fe;
	struct dvb_tuner_ops *tuner_ops = &state->fe.ops.tuner_ops;
	u32 status, i;
	
	if (tuner_ops->get_status == 0)
		return 0;
	
	for (i = 0; i < 10; i++) {
		tuner_ops->get_status(fe, &status);
		
		if (status)
			return 0;
		
		usleep_release(5000);
	}

	return -EIO;
}

static void setupSR(struct stv0910_state *state, enum stv0910_algo algo) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	s32 srinit, srup, srlow, tol;
	u8  r[6];

	srinit = ((u64) p->symbol_rate << 16) / state->base->mclk;
	
	if (algo == WarmStart || algo == ColdStart) {
		tol   = srinit >> 7;
		srup  = srinit + tol;
		srlow = srinit - tol;
	}
	else {
		if (p->scan_sr_max == 0)
			p->scan_sr_max = state->fe.ops.info.symbol_rate_max;
		
		if (p->scan_sr_min == 0)
			p->scan_sr_min = state->fe.ops.info.symbol_rate_min;
		
		srup  = ((u64) p->scan_sr_max << 16) / state->base->mclk;
		srlow = ((u64) p->scan_sr_min << 16) / state->base->mclk;
	}
	
	srup  = srup  > 0x7fff ? 0x7fff : srup  < 0 ? 0 : srup;
	srlow = srlow > 0x7fff ? 0x7fff : srlow < 0 ? 0 : srlow;
		
	r[0] = (srinit >> 8) & 0x7f;
	r[1] = srinit;
	r[2] = (srup   >> 8) & 0x7f;
	r[3] = srup;
	r[4] = (srlow  >> 8) & 0x7f;
	r[5] = srlow;
	
	WRITEregs(SFRINIT1, r, 6);
	dprintk("%s: init %u up %u low %u\n", __func__, srinit, srup, srlow);
}

static void setupCF(struct stv0910_state *state, u32 searchRange) {
	u8  r[4];
	s16 cfup, cflow;
	
	if (searchRange > 72000000)
		searchRange = 72000000;
	
	cfup  = ((u64) (searchRange >> 1) << 16) / state->base->mclk;
	cflow = -cfup;
		
	r[0] = cfup >> 8;
	r[1] = cfup;
	
	WRITEregs(CFRUP1, r, 2);
	
	r[0] = cflow >> 8;
	r[1] = cflow;
	r[2] = 0x00;
	r[3] = 0x00;

	WRITEregs(CFRLOW1, r, 4);
	dprintk("%s: up %d low %d\n", __func__, cfup, cflow);
}

static int setupDemod(struct stv0910_state *state, enum stv0910_algo algo, u32 searchRange) {
	s32 status;
	u8  regs[2];
	
	WRITEreg(DEMOD, 0x00);
	
	if ((status = setSearchStandard(state)) || (status = setViterbi(state)) || (status = setModCodes(state)))
		return status;
		
	regs[0] = 0x0b;
	regs[1] = 0x0a;
	
	WRITEregs(ACLC2S2Q, regs, 2);
	
	regs[0] = 0x84;
	regs[1] = 0x84;
	
	WRITEregs(BCLC2S2Q, regs, 2);
	
	regs[0] = 0x79;
	regs[1] = 0x1c;
	
	WRITEregs(CARFREQ, regs, 2);
	WRITEreg(CARCFG, 0x46);
	
	setupSR(state, algo);
	setupCF(state, searchRange);

	WRITEreg(DMDISTATE, 0x1f);
	
	return 0;
}

static void stopDemod(struct stv0910_state *state) {
	u8 pdelctrl1 = READreg(PDELCTRL1);
	
	controlTS(state, 0);
	WRITEreg(PDELCTRL1, INSERTfield(ALGOSWRST, pdelctrl1, 1));
	WRITEreg(PDELCTRL1, INSERTfield(ALGOSWRST, pdelctrl1, 0));
	WRITEreg(AGC2O, 0x5b);
	WRITEreg(DMDISTATE, 0x5c);
}

static enum stv0910_search_status algoSetup(struct stv0910_state *state, enum stv0910_algo algo, u32 searchRange) {
	int result = 0;
	u8 aep;
	
	stopDemod(state);
	
	if ((result = tune(state, searchRange)))
		return result;
	
	if ((result = setupDemod(state, algo, searchRange)))
		return result;
	
	if ((result = waitTunerLock(state)))
		return result;
	
	switch (algo) {
		case WarmStart:
			aep = 0x18;
			break;
			
		case ColdStart:
			aep = 0x15;
			break;
			
		default:
			aep = 0x00;
			break;
	}
	
	WRITEreg(DMDISTATE, aep);
	
	return NoCarrier;
}

static enum stv0910_search_status getTimingLock(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u8  status;
	int time;
	
	for (time = 0;; time += 10) {
		status = READreg(DSTATUS);
		
		if (EXTRACTfield(CAR_LOCK, status) && (EXTRACTfield(TMGLOCK_QUALITY, status) & 0x2)) {
			p->lock_time = time;
			dprintk("%s: lock time %d\n", __func__, time);
			return NoData;
		}
		
		if (time >= state->demodTimeout)
			break;
		
		usleep_release(10000);
	}
	
	return NoTiming;
}

static enum stv0910_search_status getFullLock(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u8  status;
	int time;
	
	for (time = 0;; time += 10) {
		status = READreg(DSTATUS);
		
		if (EXTRACTfield(LOCK_DEFINITIF, status)) {
			p->lock_time += time;
			dprintk("%s: lock time %d\n", __func__, time);
			return SearchOK;
		}
		
		if (time >= state->fecTimeout)
			break;
		
		usleep_release(10000);
	}
	
	return NoData;
}

static enum stv0910_search_status algo(struct stv0910_state *state, enum stv0910_algo algo, u8 scanMode);

static enum stv0910_search_status optimizeTuning(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	
	p->frequency   = getFrequency(state);
	p->symbol_rate = getSymbolRate(state);
	
	return algo(state, WarmStart, 0);
}

static void getSignalParameters(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	const char *delsys;
	u8 dmdmodcod;
	
	updateStatus(&state->fe);
	
	switch (p->delivery_system) {
		case SYS_DVBS2:
			delsys          = "DVB-S2";
			dmdmodcod       = READreg(DMDMODCOD);
			state->modCode  = EXTRACTfield(DEMOD_MODCOD, dmdmodcod);
			state->frame    = EXTRACTfield(SHORT_FRAME, dmdmodcod);
			p->modulation   = mod_to_user[state->modCode];
			p->fec_inner    = s2_modcode_to_user[state->modCode];
			p->pilot        = EXTRACTfield(PILOT_PRESENT, dmdmodcod);
			p->rolloff      = roll_to_user[READfield(ROLLOFF_STATUS)];
			p->frame_length = s12_frame_length[state->modCode][state->frame];
			break;
			
		case SYS_DVBS:
		case SYS_DSS:
			delsys          = p->delivery_system == SYS_DVBS ? "DVB-S" : "DSS";
			dmdmodcod       = READfield(VIT_CURPUN);
			state->modCode  = 0;
			state->frame    = 0;
			p->modulation   = QPSK;
			p->fec_inner    = s_fec_to_user[dmdmodcod];
			p->pilot        = 0;
			p->rolloff      = ROLLOFF_35;
			p->frame_length = s12_frame_length[dmdmodcod][2];
			break;
			
		default:
			delsys          = "Unknown";
			state->modCode  = 0;
			state->frame    = 0;
			p->modulation   = MOD_UNKNOWN;
			p->fec_inner    = FEC_NONE;
			p->pilot        = 0;
			p->rolloff      = ROLLOFF_UNKNOWN;
			p->frame_length = 0;
			break;
	}
	
	p->frequency    = getFrequency(state);
	p->symbol_rate  = getSymbolRate(state);
	p->snr          = getSNR(state);
	p->signal_level = getSignalLevel(state);
	
	dprintk("%s: %s freq %lld sr %d modcode %d mod %d fec %d pilot %d roll %d short frame %d frame length %d\n", 
			__func__, delsys, p->frequency, p->symbol_rate, state->modCode, p->modulation, p->fec_inner, p->pilot, 
			p->rolloff, state->frame, p->frame_length);
}

static void optimizeTracking(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	u8 aclc     = 0;
	u8 dmdcfgmd = READreg(DMDCFGMD) & ~0xc0;
	
	switch (p->delivery_system) {
		case SYS_DVBS2:
			dmdcfgmd |= 0x80;
			
			if (state->frame == LongFrame) {
				aclc = getOptimCarLoop(state);
				
				if (state->modCode <= DVBS2_QPSK_9_10)
					WRITEreg(ACLC2S2Q, aclc);
				else if (state->modCode <= DVBS2_8PSK_9_10) {
					WRITEreg(ACLC2S2Q, 0x2a);
					WRITEreg(ACLC2S28, aclc);
				} else if (state->modCode <= DVBS2_16APSK_9_10) {
					WRITEreg(ACLC2S2Q,   0x2a);
					WRITEreg(ACLC2S216A, aclc);
				} else if (state->modCode <= DVBS2_32APSK_9_10) {
					WRITEreg(ACLC2S2Q,   0x2a);
					WRITEreg(ACLC2S232A, aclc);
				}
			}
			break;
			
		case SYS_DVBS:
		case SYS_DSS:
			dmdcfgmd |= 0x40;
			break;
			
		default:
			p->frame_length |= 0xc0;
			break;
	}
	
	WRITEreg(DMDCFGMD, dmdcfgmd);
}

static void setupCounters(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	
	if (p->delivery_system == SYS_DVBS2) {
		u8 pdelctrl2 = READreg(PDELCTRL2) & ~0x40;
		WRITEreg(PDELCTRL2, pdelctrl2 | 0x40);
		WRITEreg(PDELCTRL2, pdelctrl2);
	}
	
	state->berScale  = p->error_rate == ERROR_PER ? 0x0 : 0x2;
	state->berSource = error_source_select[p->error_rate];
	
	WRITEreg(ERRCTRL1, state->berSource       | state->berScale);
	WRITEreg(ERRCTRL2, STV0910_ERR_SOURCE_PER | 0x0);
	WRITEreg(FBERCPT4, 0x00);
}

static void wrapupSearch(struct stv0910_state *state) {
	getSignalParameters(state);
	optimizeTracking(state);
	controlTS(state, 1);
	setupCounters(state);
}

static enum stv0910_search_status blindScanAlgo(struct stv0910_state *state) {
	dprintk("%s: not implemented\n", __func__);
	return Failure;
}

static enum stv0910_search_status blindScanAlgoAEP(struct stv0910_state *state) {
	dprintk("%s: not implemented\n", __func__);
	return Failure;
}

static enum stv0910_search_status algo(struct stv0910_state *state, enum stv0910_algo algo, u8 scanMode) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	enum stv0910_search_status status = NoCarrier;
	u32 searchRange;
	
	getLockTimeouts(state);
	
	if (scanMode) {
		p->single_tune = 1;
		
		switch (algo) {
			case BlindSearch:
			default:
				status = blindScanAlgo(state);
				break;
				
			case BlindScanAEP:
				status = blindScanAlgoAEP(state);
				break;
		}
	}
	else {
		switch (algo) {
			case ColdStart:
				searchRange = 2 * 10000000;
				break;
				
			case WarmStart:
				searchRange = 2 * 1000000;
				
				if (state->demodTimeout > 1000) {
					state->demodTimeout = 1000;
					state->fecTimeout   =  250;
				}
				break;
				
			case BlindSearch:
			default:
				searchRange = 2 * 36000000;
				break;
		}
		
		if ((status = algoSetup(state, algo, searchRange)) != NoCarrier)
			return status;
		
		if ((status = getTimingLock(state)) != NoData)
			return status;
			
		if (algo != WarmStart)
			return optimizeTuning(state);
			
		if ((status = getFullLock(state)) != SearchOK)
			return status;
		
		wrapupSearch(state);
	}
	
	return status;
}

static enum dvbfe_search search(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;
	struct dtv_frontend_properties *p = &fe->dtv_property_cache;
	enum stv0910_algo searchAlgo;
	u8  scanMode;
	
	if (demodIsDead(state))
		return -EIO;
	
	switch (p->algo) {
		case ALGO_COLDSTART:
			scanMode       = 0;
			searchAlgo     = ColdStart;
			break;
			
		case ALGO_WARMSTART:
			scanMode       = 0;
			searchAlgo     = WarmStart;
			break;
			
		case ALGO_BLINDSCAN:
			scanMode       = 1;
			searchAlgo     = BlindSearch;
			p->symbol_rate = p->scan_sr_min;
			break;
			
		case ALGO_EXPERIMENTAL:
			scanMode       = 1;
			searchAlgo     = BlindScanAEP;
			break;
			
		case ALGO_BLIND:
		case ALGO_FAST:
		case ALGO_DEFAULT:
		default:
			scanMode       = 0;
			searchAlgo     = BlindSearch;
			break;
	}
		
	if (p->stream_id == NO_STREAM_ID_FILTER) {
		setPLS(state, 0, 0);
		setMIS(state, -1);
	}
	else {
		setPLS(state, p->stream_id >> 26, p->stream_id >> 8);
		setMIS(state, p->stream_id);
	}
	
	return algo(state, searchAlgo, scanMode) == SearchOK ? DVBFE_ALGO_SEARCH_SUCCESS : DVBFE_ALGO_SEARCH_FAILED;
}	

static void calculateBER(struct stv0910_state *state, u32 denom) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;

	u32 num = READbe(ERRCNT12, 3);
	
	if (!(num >> 23)) {
		denom <<= state->berScale << 1;
		p->ber  = denom ? (u64) num * 1000000000LL / denom : 0;
		
		if (!(num >> 14) && state->berScale < 6) {
			state->berScale++;
			WRITEreg(ERRCTRL1, state->berSource | state->berScale);
		} else if ((num >> 19) && state->berScale > 2) {
			state->berScale--;
			WRITEreg(ERRCTRL1, state->berSource | state->berScale);
		}
	}
}

static void calculatePER(struct stv0910_state *state) {
	struct dtv_frontend_properties *p = &state->fe.dtv_property_cache;
	
	u32 denom = READbe64(FBERCPT4, 5);
	u32 num   = READbe(ERRCNT12, 3) & 0x7fffff;
	
	WRITEreg(FBERCPT4, 0x00);
	
	p->ber = denom ? (u64) num * 1000000000LL / denom : 0;
}

static int updateBER(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;
	struct dtv_frontend_properties *p = &fe->dtv_property_cache;
	
	if (state->berSource == error_source_select[p->error_rate]) {
		switch (error_unit[p->error_rate][p->delivery_system == SYS_DVBS2]) {
			case BaseByte:
				calculateBER(state, 8 << 10);
				break;
				
			case BasePacket:
				calculatePER(state);
				break;
				
			case BaseFrame:
			default:
				calculateBER(state, p->frame_length << 0);
				break;
		}
	}
	else {
		setupCounters(state);
		p->ber = 0;
	}
	
	return 0;
}

static int updateUncBlocks(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;
	struct dtv_frontend_properties *p = &fe->dtv_property_cache;
	
	p->ucblocks = READbe(ERRCNT22, 3);
	
	return 0;
}

static int updateBitrate(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;
	struct dtv_frontend_properties *p = &fe->dtv_property_cache;
	
	p->bitrate = ((u64) state->config->mclk * READbe(TSBITRATE1, 2) + (1 << 13)) >> 14;
	
	return 0;
}

static void initDiSEqC(struct stv0910_state *state) {
	u8 f22k = ((state->base->mclk + 11000 * 32) / (22000 * 32));
	u8 distxcfg = INSERTfield(DISEQC_MODE, state->disTXcfg, 0x2);
	
	WRITEreg(DISRXCFG, INSERTfield(DISRX_RESET, state->disRXcfg, 0x1));
	WRITEreg(DISRXCFG, state->disRXcfg);
	WRITEreg(DISTXCFG, INSERTfield(DISTX_RESET, distxcfg, 0x1));
	WRITEreg(DISTXCFG, distxcfg);
	WRITEreg(DISTXF22, f22k);
}

static int setTone(struct dvb_frontend *fe, enum fe_sec_tone_mode tone) {
	struct stv0910_state *state = fe->demodulator_priv;
	
	switch (tone) {
		case SEC_TONE_ON:
			return WRITEreg(DISTXCFG, INSERTfield(DISEQC_MODE, state->disTXcfg, 0x0));
			
		case SEC_TONE_OFF:
			return WRITEreg(DISTXCFG, INSERTfield(DISEQC_MODE, state->disTXcfg, 0x2));
			
		default:
			break;
	}
	
	return -EINVAL;
}

static void waitDiSEqC(struct stv0910_state *state) {
	u32 bytesLeft = READfield(TXFIFO_BYTES);
	u32 gapWait   = EXTRACTfield(TIM_CMD, state->disTXcfg) * 5 + 15 + 50;
	int i;
	
	if (bytesLeft)
		usleep(bytesLeft * 13500);
		
	for (i = 0; i < gapWait; i += 5) {
		if (!READfield(GAP_BURST))
			return;
		
		usleep(5000);
	}
}

static int sendMasterCmd(struct dvb_frontend *fe, struct dvb_diseqc_master_cmd *cmd){
	struct stv0910_state *state = fe->demodulator_priv;
	u8 disTXcfg = INSERTfield(DISEQC_MODE, state->disTXcfg, 0x2);
	int i;
	
	WRITEreg(DISRXCFG, INSERTfield(DISRX_ON, state->disRXcfg, 0x0));
	WRITEreg(DISTXCFG, INSERTfield(DIS_PRECHARGE, disTXcfg, 0x1));
		
	//	stv0910 DiSEqC FIFO is large enough to accommodate all legal commands
	for (i = 0; i < cmd->msg_len; i++)
		WRITEreg(DISTXFIFO, cmd->msg[i]);
	
	WRITEreg(DISTXCFG, disTXcfg);
	waitDiSEqC(state);
	WRITEreg(DISRXCFG, INSERTfield(DISRX_ON, state->disRXcfg, 0x1));
	
	return 0;
}

static int sendBurst(struct dvb_frontend *fe, enum fe_sec_mini_cmd burst) {
	struct stv0910_state *state = fe->demodulator_priv;
	int r = 0;
	
	WRITEreg(DISRXCFG, INSERTfield(DISRX_ON, state->disRXcfg, 0x0));

	switch (burst) {
		case SEC_MINI_A:
			WRITEreg(DISTXCFG, INSERTfield(DISEQC_MODE, state->disTXcfg, 0x3));
			WRITEreg(DISTXFIFO, 0x00);
			waitDiSEqC(state);
			break;
			
		case SEC_MINI_B:
			WRITEreg(DISTXCFG, INSERTfield(DISEQC_MODE, state->disTXcfg, 0x2));
			WRITEreg(DISTXFIFO, 0xff);
			waitDiSEqC(state);
			break;
			
		default:
			r = 1;
			break;
	}
	
	WRITEreg(DISRXCFG, INSERTfield(DISRX_ON, state->disRXcfg, 0x1));

	return r;
}

static int receiveSlaveReply(struct dvb_frontend *fe, struct dvb_diseqc_slave_reply *reply) {
	struct stv0910_state *state = fe->demodulator_priv;
	s32 timeout;
	u8 end, count;
	
	reply->msg_len = 0;
	timeout = reply->timeout;
	
	if (timeout <= 0 || timeout > 10000)
		timeout = 100;
	
	while (READfield(RXFIFO_BYTES) == 0) {
		if ((timeout -= 10) < 0)
			return -EIO;
		
		usleep(10000);
	}
	
	timeout = 100;
	
	while (1) {
		end = READfield(RXEND);
		
		for (count = READfield(RXFIFO_BYTES); count > 0; count--) {
			if (reply->msg_len == sizeof(reply->msg))
				return -EIO;
			
			reply->msg[reply->msg_len++] = READreg(DISRXFIFO);
		}
		
		if (end)
			break;
		
		if ((timeout -= 10) < 0)
			return -EIO;
			
		usleep(10000);
	}
	
	return 0;
}

static void setMasterClock(struct stv0910_state *state) {
	u8  reg[3];
	u32 idf  = 1;
	u32 odf  = 4;
	u32 cp   = 7;
	u32 ndiv = (state->config->mclk * odf * idf) / state->config->extclk;
	
	if (ndiv >= 72 && ndiv <= 225)
		cp = (ndiv >> 3) - 1;
	
	reg[0] = (cp << 3) | idf;
	reg[1] = ndiv;
	reg[2] = odf;
	
	writeRegs(state, RSTV0910_NCOARSE, reg, 3);
	
	state->base->mclk = (state->config->extclk * ndiv) / (odf * idf);
	
	readRegs(state, RSTV0910_NCOARSE, reg, 3);
}

static int init_regs(struct stv0910_state *state, const struct stv0910_init_table *table) {
	if (table) {
		int i;
		init_regs(state, table->next);
	
		for (i = 0; i < table->size; i++)
			writeReg(state, table->init[i].reg, table->init[i].val);
	}
	
	return 0;
}

static int sleep(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;
	struct stv0910_base *base = state->base;
	const struct stv0910_config *config = state->config;
	u8 demod = config->demod;
	
	dprintk("%s: STV0910\n", __func__);
	
	if (!base->sleeping[demod]) {
		stopDemod(state);
		setDemodMutex(state, 1);
		
		writeField(state, demod ? FSTV0910_STOP_DVBS2FEC2 : FSTV0910_STOP_DVBS2FEC, 1);
		writeField(state, demod ? FSTV0910_STOP_DVBS1FEC2 : FSTV0910_STOP_DVBS1FEC, 1);
		writeField(state, demod ? FSTV0910_STOP_DEMOD2    : FSTV0910_STOP_DEMOD,    1);
		writeField(state, demod ? FSTV0910_ADC2_PON       : FSTV0910_ADC1_PON,      0);

		if (config->single || base->sleeping[1 - demod]) {
			writeField(state, FSTV0910_FSK_PON,        0);
			writeField(state, FSTV0910_I2C_DISEQC_PON, 0);
			writeField(state, FSTV0910_STANDBY,        1);
			
			base->initDone = 0;
		}
		
		base->sleeping[demod] = 1;
		setDemodMutex(state, 0);
	}
	
	return 0;
}

static int init(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;
	struct stv0910_base *base = state->base;
	const struct stv0910_config *config = state->config;
	u8 demod = config->demod;
	
	if (demodIsDead(state)) {
		dprintk("%s: STV0910 is dead\n", __func__);
		return -EINVAL;
	}
	
	dprintk("%s: STV0910\n", __func__);
	
	setDemodMutex(state, 1);	

	if (!base->initDone) {
		writeField(state, FSTV0910_STANDBY,        0);
		writeField(state, FSTV0910_I2C_DISEQC_PON, 1);
		writeField(state, FSTV0910_FSK_PON,        1);

		init_regs(state, &demodInit0);
		init_regs(state, config->init);

		setMasterClock(state);

		if (config->single)
			writeReg(state, RSTV0910_GENCFG, 0x14);

		writeReg(state, RSTV0910_TSTRES0, 0x80);
		writeReg(state, RSTV0910_TSTRES0, 0x00);
		
		base->initDone = 1;
	}
	
	writeField(state, demod ? FSTV0910_ADC2_PON       : FSTV0910_ADC1_PON,      1);
	writeField(state, demod ? FSTV0910_STOP_DEMOD2    : FSTV0910_STOP_DEMOD,    0);
	writeField(state, demod ? FSTV0910_STOP_DVBS1FEC2 : FSTV0910_STOP_DVBS1FEC, 0);
	writeField(state, demod ? FSTV0910_STOP_DVBS2FEC2 : FSTV0910_STOP_DVBS2FEC, 0);
	
	base->sleeping[demod] = 0;
	setDemodMutex(state, 0);

	state->i2crpt   = INSERTfield(ENARPT_LEVEL,  READreg(I2CRPT)   & 0x78, config->rptlvl);
	state->tscfgh   = INSERTfield(TSFIFO_SERIAL, READreg(TSCFGH)   & 0xfe, config->serial);
	state->disRXcfg = INSERTfield(PINSELECT,     READreg(DISRXCFG) & 0x7e, config->disrxpin);
	state->disTXcfg = READreg(DISTXCFG) & 0x78;
	
	WRITEreg(I2CRPT, state->i2crpt | 0x02);
	WRITEreg(TSCFGH, state->tscfgh | 0x01);
	WRITEreg(TSCFGH, state->tscfgh | 0x00);
	
	initDiSEqC(state);

	return 0;
}


static void release(struct dvb_frontend *fe) {
	struct stv0910_state *state = fe->demodulator_priv;

	if (--state->base->count == 0) {
		list_del(&state->base->stv0910list);
		kfree(state->base);
	}
	
	kfree(state);
}

static struct dvb_frontend_ops stv0910_ops = {
	.info = {
		.name					= "STV0910 Demodulator",
		.frequency_min			=  240000000,
		.frequency_max			= 2160000000,
		.frequency_stepsize		=         57,
		.symbol_rate_min		=     100000,
		.symbol_rate_max		=   67500000,
		.caps =	FE_CAN_INVERSION_AUTO |
				FE_CAN_FEC_1_2 | FE_CAN_FEC_2_3 |
				FE_CAN_FEC_3_4 | FE_CAN_FEC_5_6 |
				FE_CAN_FEC_7_8 | FE_CAN_QPSK    |
				FE_CAN_2G_MODULATION |
				FE_CAN_RECOVER | FE_CAN_FEC_AUTO
	},
	.delsys = { SYS_S_AUTO, SYS_DVBS, SYS_DVBS2, SYS_DSS },
	.release					= release,
	.init						= init,
	.sleep						= sleep,
	.get_frontend_algo			= getAlgo,
	.i2c_gate_ctrl				= i2cGateCtrl,
	.diseqc_send_master_cmd		= sendMasterCmd,
	.diseqc_send_burst			= sendBurst,
	.diseqc_recv_slave_reply	= receiveSlaveReply,
	.set_tone					= setTone,
	.search						= search,
	.update_ber					= updateBER,
	.update_bitrate				= updateBitrate,
	.update_frequency			= updateFrequency,
	.update_signal_strength		= updateSignalStrength,
	.update_snr					= updateSNR,
	.update_status				= updateStatus,
	.update_symbol_rate			= updateSymbolRate,
	.update_ucblocks            = updateUncBlocks,
};

static struct stv0910_base *matchBase(struct i2c_adapter *i2c, u8 adr) {
	struct stv0910_base *p;

	list_for_each_entry(p, &stv0910list, stv0910list)
		if (p->i2c == i2c && p->adr == adr)
			return p;
	
	return 0;
}

struct dvb_frontend *stv0910_attach(struct i2c_adapter *i2c, const struct stv0910_config *config) {
	struct stv0910_state *state;
	struct stv0910_base *base;
	struct dvb_frontend *fe;
	
	if (!(state = kzalloc(sizeof(struct stv0910_state), GFP_KERNEL)))
		return 0;

	if (!(base = matchBase(i2c, config->adr))) {
		if (!(base = kzalloc(sizeof(struct stv0910_base), GFP_KERNEL)))
			goto fail;
		
		base->i2c = i2c;
		base->adr = config->adr;

		mutex_init(&base->tunerLock);
		mutex_init(&base->demodLock);
		
		list_add(&base->stv0910list, &stv0910list);
	}
	
	base->count++;
	fe = &state->fe;
	
	state->config = config;
	state->base   = base;
	
	fe->ops              = stv0910_ops;
	fe->demodulator_priv = state;

	if (init(fe) == 0) {
		sleep(fe);
		return fe;
	}
	
	kfree(base);

fail:
	kfree(state);
	return 0;
}
EXPORT_SYMBOL_GPL(stv0910_attach);

MODULE_DESCRIPTION("STV0910 driver");
MODULE_AUTHOR("Ralph Metzler, Manfred Voelkel");
MODULE_LICENSE("GPL");
